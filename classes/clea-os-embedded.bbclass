IMAGE_FSTYPES = "tar.bz2 wic wic.bmap wic.gz wic.bz2 ext4"
IMAGE_FSTYPES:append:seco-mtk = " wic.img aiotflash.tar"
IMAGE_FSTYPES:remove:qemuall = "wic wic.bmap wic.gz wic.bz2"

WKS_FILE:rpi = "image-rpi.wks.in"
WKS_FILE:seco-mx6 = "image-imx6.wks.in"
WKS_FILE:seco-mx8 = "image-imx8.wks.in"
WKS_FILE:seco-intel = "image-intel.wks.in"
WKS_FILE:seco-mtk = "image-genio.wks.in"

IMAGE_TYPEDEP_wic:seco-intel = "ext4"
INITRD_IMAGE_LIVE:seco-intel = "core-image-minimal-initramfs"
IMAGE_BOOT_FILES:seco-intel:append = "\
    ${KERNEL_IMAGETYPE} \
    microcode.cpio \
    ${IMGDEPLOYDIR}/${IMAGE_BASENAME}-${MACHINE}.ext4;rootfs.img \
    ${@bb.utils.contains('EFI_PROVIDER', 'grub-efi', 'grub-efi-bootx64.efi;EFI/BOOT/bootx64.efi', '', d)} \
    ${@bb.utils.contains('EFI_PROVIDER', 'grub-efi', '${IMAGE_ROOTFS}/boot/EFI/BOOT/grub.cfg;EFI/BOOT/grub.cfg', '', d)} \
    ${@bb.utils.contains('EFI_PROVIDER', 'systemd-boot', 'systemd-bootx64.efi;EFI/BOOT/bootx64.efi', '', d)} \
    ${@bb.utils.contains('EFI_PROVIDER', 'systemd-boot', '${IMAGE_ROOTFS}/boot/loader/loader.conf;loader/loader.conf ', '', d)} \
    ${@bb.utils.contains('EFI_PROVIDER', 'systemd-boot', '${IMAGE_ROOTFS}/boot/loader/entries/boot.conf;loader/entries/boot.conf', '', d)} "

DO_IMAGE_WIC_EXTRA_DEP = ""
DO_IMAGE_WIC_EXTRA_DEP:seco-intel:append = " ${INITRD_IMAGE_LIVE}:do_image_complete ${IMAGE_BASENAME}:do_image_ext4 " 
do_image_wic[depends] += "${DO_IMAGE_WIC_EXTRA_DEP}"
do_rootfs[depends] += "${@bb.utils.contains('WKS_FILE', 'image-intel.wks.in', 'virtual/kernel:do_deploy', '', d)}"
