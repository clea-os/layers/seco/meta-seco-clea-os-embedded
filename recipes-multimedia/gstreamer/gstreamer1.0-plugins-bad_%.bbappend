
PACKAGECONFIG:remove = " \
    ${@bb.utils.contains('DISTRO_FEATURES', 'x11', '', \
        bb.utils.contains('DISTRO_FEATURES', 'wayland', \
            '', 'vulkan', d), d)} \
"

PACKAGECONFIG:append = " \
    curl \
     ${@bb.utils.contains('LICENSE_FLAGS_ACCEPTED', 'commercial', 'libde265', '', d)} \
     v4l2codecs \
     webrtc \
"

