DESCRIPTION = "Clea OS splash bmp for U-boot"
LICENSE = "CLOSED"

FILESEXTRAPATHS:prepend := "${THISDIR}/files:"
SRC_URI += "file://splash.bmp"

inherit deploy nopackages

do_deploy() {
    cp ${WORKDIR}/splash.bmp ${DEPLOYDIR}
}

addtask deploy after do_compile