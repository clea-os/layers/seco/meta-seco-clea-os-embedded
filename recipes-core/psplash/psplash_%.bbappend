FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

DEPENDS += "gdk-pixbuf-native"

SRC_URI += "file://psplash-quit.service file://psplash-clea-os-img.h file://psplash-clea-os-colors.patch file://psplash-bar-img.png"

SPLASH_IMAGES = "file://psplash-clea-os-img.h;outsuffix=default"

# The core psplash recipe is only designed to deal with modifications to the
# 'logo' image; we need to change the bar image too, since we are changing
# colors
do_configure:append () {
	cd ${S}
	# strip the -img suffix from the bar png -- we could just store the
	# file under that suffix-less name, but that would make it confusing
	# for anyone updating the assets
	cp ../psplash-bar-img.png ./psplash-bar.png
	./make-image-header.sh ./psplash-bar.png BAR
}

# This service works as a timeout for the splash screen
do_install:append() {
    if ${@bb.utils.contains('DISTRO_FEATURES', 'systemd', 'true', 'false', d)}; then
        install -m 644 ${WORKDIR}/psplash-quit.service ${D}/${systemd_system_unitdir}
    fi
}

SYSTEMD_SERVICE:${PN} += "${@bb.utils.contains('PACKAGECONFIG', 'systemd', 'psplash-quit.service', '', d)}"