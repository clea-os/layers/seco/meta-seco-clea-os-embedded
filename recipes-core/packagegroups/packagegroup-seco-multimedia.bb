SUMMARY = "SECO Multimedia Packagegroup"
DESCRIPTION = "Packagegroup for multimedia related packages, such as media players and codecs"

inherit packagegroup

PACKAGES = " \
    ${PN} \
    ${PN}-browser \
"

MACHINE_MULTIMEDIA_PLUGIN ?= ""

RDEPENDS:${PN} = " \
    alsa-utils \
    ${@bb.utils.contains('LICENSE_FLAGS_ACCEPTED', 'commercial',  \
        'packagegroup-seco-gstreamer1.0-commercial', 'packagegroup-seco-gstreamer1.0', d)} \
    ${MACHINE_MULTIMEDIA_PLUGIN} \
"

RDEPENDS:${PN}-browser = " \
    ${@bb.utils.contains('DISTRO_FEATURES', 'wayland', 'chromium-ozone-wayland libv4l', \
       bb.utils.contains('DISTRO_FEATURES', 'x11', 'chromium-x11 libv4l', \
                                                    '', d), d)} \
"
