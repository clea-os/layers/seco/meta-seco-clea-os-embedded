DESCRIPTION = "SECO Graphics Packagegroup"
SUMMARY = ""

inherit packagegroup

MACHINE_HW_GRAPHICS_PLUGIN ?= ""

PACKAGES = " \
    ${PN} \
    ${@bb.utils.contains('DISTRO_FEATURES', 'wayland', '${PN}-weston',  \
	bb.utils.contains('DISTRO_FEATURES',     'x11', '${PN}-x11', \
						'', d), d)} \
"

RDEPENDS:${PN} = " \
    ${MACHINE_HW_GRAPHICS_PLUGIN} \
    ${@bb.utils.contains('DISTRO_FEATURES', 'wayland', '${PN}-weston', '', d)} \
    tslib-calibrate \
"

RDEPENDS:${PN}-weston = " \
    weston \
    weston-init \
"

RDEPENDS:${PN}-x11 = " \
    xf86-video-modesetting \
    xserver-xorg-module-exa \
"
