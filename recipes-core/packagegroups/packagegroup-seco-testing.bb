SUMMARY = "SECO Test Packagegroup"
DESCRIPTION = "Packagegroup for hardware testing"

# Prevents the dynamic renaming of packages
# (which throws an error in newer Bitbake versions)
PACKAGE_ARCH = "${TUNE_PKGARCH}"

inherit packagegroup

PACKAGES = " \
    ${PN} \
    ${PN}-base \
    ${PN}-network \
    ${PN}-graphics \
    ${PN}-multimedia \
"

MACHINE_BASIC_TESTING_PLUGIN ?= ""
MACHINE_GRAPHICS_TESTING_PLUGIN ?= ""

GRAPHICS_X11_TESTING_PLUGIN = " \
    mesa-demos \
    glmark2 \
"

GRAPHICS_WAYLAND_TESTING_PLUGIN = " \
    weston-examples \
    glmark2 \
"

RDEPENDS:${PN}-base = " \
    devmem2 \
    io \
    evtest \
    i2c-tools \
    memtester \
    stress \
    pciutils \
    usbutils \
    canutils \
    e2fsprogs \
    lmbench \
    mmc-utils \
    spidev-test \
    libgpiod-tools \
    bonnie++ \
    dbench \
    fio \
    iozone3 \
    nbench-byte \
    tiobench \
    util-linux \
    util-linux-lscpu \
    mtd-utils \
    minicom \
    setserial \
    bmap-tools \
    spitools \
    ${@bb.utils.contains('LINUXLIBCVERSION', '4.19%', '', 'mdio-tools', d)} \
    ${@bb.utils.contains('TUNE_FEATURES', 'neon', 'cpuburn-neon', \
        bb.utils.contains('TUNE_FEATURES', 'cortexa53 crypto', 'cpuburn-neon', \
            '', d), d)} \
    ${MACHINE_BASIC_TESTING_PLUGIN} \
"

RDEPENDS:${PN}-network = " \
    iperf3 \
    ethtool \
    curl \
    ${@bb.utils.contains('MACHINE_FEATURES', 'wifi', 'hostapd', '', d)} \
"

RDEPENDS:${PN}-graphics = " \
    fb-test \
    fbgrab \
    fbset-modes \
    fbv \
    tslib-tests \
    ${@bb.utils.contains('DISTRO_FEATURES', 'x11', '${GRAPHICS_X11_TESTING_PLUGIN}', \
        bb.utils.contains('DISTRO_FEATURES', 'wayland', \
            '${GRAPHICS_WAYLAND_TESTING_PLUGIN}', '', d), d)} \
    ${MACHINE_GRAPHICS_TESTING_PLUGIN} \
"

RDEPENDS:${PN}-multimedia = " \
    ${@bb.utils.contains('DISTRO_FEATURES', 'x11', 'gst-examples',  \
        bb.utils.contains('DISTRO_FEATURES', 'wayland', 'gst-examples', '',  d), d)} \
    v4l-utils \
"

RDEPENDS:${PN} = " \
    ${PN}-base \
    ${PN}-graphics \
    ${PN}-network  \
    ${PN}-multimedia \
"
