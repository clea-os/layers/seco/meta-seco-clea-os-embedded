SUMMARY = "SECO Debug Packagegroup"
DESCRIPTION = "Packagegroup for debugging software"

# Prevents the dynamic renaming of packages
# (which throws an error in newer Bitbake versions)
PACKAGE_ARCH = "${TUNE_PKGARCH}"

inherit packagegroup

PACKAGES = " \
    ${PN} \
    ${PN}-base \
"

MACHINE_SPECIFIC_TESTING_PLUGIN ?= ""

RDEPENDS:${PN}-base = " \
    gdb \
    glibc-utils \
    valgrind \
    blktrace \
    ddrescue \
    dhrystone \
    strace \
    vim \
"

RDEPENDS:${PN} = " \
    ${PN}-base \
    ${MACHINE_SPECIFIC_TESTING_PLUGIN} \
"
