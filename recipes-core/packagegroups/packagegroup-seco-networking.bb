SUMMARY = "SECO Connectivity Packagegroup"
DESCRIPTION = "Packagegroup for network related packages"

inherit packagegroup

PACKAGES = " \
    ${PN} \
    ${@bb.utils.contains('COMBINED_FEATURES', 'modem', '${PN}-modem', '', d)} \
"

RDEPENDS:${PN} = " \
    ${@bb.utils.contains('COMBINED_FEATURES', 'modem', '${PN}-modem', '', d)} \
"

RDEPENDS:${PN}-modem = " \
    modemmanager \
"
