DESCRIPTION = ""
SUMMARY = "SECO package group - set of GStreamer 1.0 plugins with commercial licence flag"
LICENSE_FLAGS = "commercial"

inherit packagegroup

RDEPENDS:${PN} = " \
    packagegroup-seco-gstreamer1.0 \
    packagegroup-seco-gstreamer1.0-base \
    packagegroup-seco-gstreamer1.0-audio \
    packagegroup-seco-gstreamer1.0-video \
    packagegroup-seco-gstreamer1.0-network-base \
    packagegroup-seco-gstreamer1.0-debug \
"

# Plugins from the -ugly collection which require the "commercial" flag in LICENSE_FLAGS_ACCEPTED to be set
RDEPENDS:${PN} += " \
    gstreamer1.0-plugins-ugly-asf \
    gstreamer1.0-libav \
"
