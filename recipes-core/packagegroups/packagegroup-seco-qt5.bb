DESCRIPTION = "SECO Qt5 Packagegroup"
SUMMARY = ""

# Prevents the dynamic renaming of packages
# (which throws an error in newer Bitbake versions)
PACKAGE_ARCH = "${TUNE_PKGARCH}"

inherit packagegroup

PACKAGES = " \
    ${PN} \
    ${PN}-examples \
    ${PN}-tests \
    ${PN}-tools \
    ${PN}-translations \
"

# Install fonts
QT5_FONTS = "ttf-dejavu-common ttf-dejavu-sans ttf-dejavu-sans-mono ttf-dejavu-serif "

QT5_QTQUICK3D = "qtquick3d"


RDEPENDS:${PN} = " \
    qt3d \
    qtbase \
    qtcharts \
    qtconnectivity \
    qtdatavis3d \
    qtdeclarative \
    qtgamepad \
    qtgraphicaleffects \
    qtimageformats \
    qtknx \
    qtlocation \
    qtmqtt \
    qtmultimedia \
    qtnetworkauth \
    qtquickcontrols \
    qtquickcontrols2 \
    qtremoteobjects \
    qtscript \
    qtserialbus \
    qtserialport \
    qtsmarthome \
    qtsvg \
    qttools \
    qtvirtualkeyboard \
    ${@bb.utils.contains('DISTRO_FEATURES', 'wayland', 'qtwayland qtwayland-plugins', '', d)} \
    ${@bb.utils.contains('DISTRO_FEATURES', 'x11', 'libxkbcommon', '', d)} \
    qtwebchannel \
    qtwebglplugin \
    qtwebsockets \
    qtxmlpatterns \
    qwt-qt5 \
    qtsensors \
    qtscxml \
    ${QT5_FONTS} \
    ${QT5_QTQUICK3D} \
"

RDEPENDS:${PN}-tests = " \
    qt5-demo-extrafiles \
    qt5ledscreen \
    quitbattery \
    qt5everywheredemo \ 
    qt5nmapcarousedemo \
    qt5nmapper \
    cinematicexperience \
    cinematicexperience-tools \
    qtsimplepaint \
"

RDEPENDS:${PN}-tools = " \
    qtdeclarative-tools \
    qttools-tools \
"

RDEPENDS:${PN}-translations = " \
    qttranslations-qtbase \
    qttranslations-qtconnectivity \
    qttranslations-qtlocation \
    qttranslations-qtmultimedia \
    qttranslations-qtquickcontrols \
    qttranslations-qtquickcontrols2 \
    qttranslations-qtserialport \
    qttranslations-qtwebsockets \
    qttranslations-qtwebengine \
    qttranslations-qtxmlpatterns \
    qttranslations-designer \
    qttranslations-linguist \
    qttranslations-qtscript \
    qttranslations-qthelp \
    qttranslations-assistant \ 
    qttranslations-qtdeclarative \
"

RRECOMMENDS:${PN}-examples = " \
    qtbase-examples \
    qtmqtt-examples \
    qtwebsockets-examples \
    qtdatavis3d \
    qtdatavis3d-examples \
    qtcharts-examples \
    qtknx-examples \
    qtmultimedia-examples \
    qtnetworkauth-examples \
    qtremoteobjects-examples \
    qtscxml-examples \
    qtsensors-examples \
    qtserialbus-examples \
    qtserialport-examples \
    qtsvg-examples \
    qtvirtualkeyboard-examples \
    qtwebchannel-examples \
    qtxmlpatterns-examples \
"
