SUMMARY = "SECO Clea OS Image Full for Embedded"

require seco-image-clea-os-base.bb

inherit ${@bb.utils.contains('CLEA_OS_QT_VERSION', '5.x', 'populate_sdk_qt5', \
    bb.utils.contains('CLEA_OS_QT_VERSION', '6.x', 'populate_sdk_qt6', '', d), d)}

IMAGE_FEATURES += " \
    tools-debug \
    tools-testapps \
    ssh-server-openssh \
    ${@bb.utils.contains('DISTRO_FEATURES', 'wayland', 'weston', \
       bb.utils.contains('DISTRO_FEATURES',     'x11', 'x11-base x11-sato', \
                                                       '', d), d)} \
"


IMAGE_INSTALL_TESTING += "\
    packagegroup-seco-testing \
    packagegroup-seco-testing-network \
    packagegroup-seco-testing-graphics \
    packagegroup-seco-testing-multimedia \
"

IMAGE_INSTALL_GRAPHICS += "\
    packagegroup-seco-graphics \
    ${@bb.utils.contains('DISTRO_FEATURES', 'x11 wayland', 'weston-xwayland xterm', '', d)} \
    ${@bb.utils.contains('DISTRO_FEATURES', 'wayland', 'packagegroup-seco-graphics-weston', \
       bb.utils.contains('DISTRO_FEATURES',     'x11', 'packagegroup-seco-graphics-x11', \
                                                       '', d), d)} \
"

IMAGE_INSTALL_MULTIMEDIA += " \
    packagegroup-seco-multimedia \
    packagegroup-seco-multimedia-browser \
    packagegroup-seco-gstreamer1.0 \
    packagegroup-seco-gstreamer1.0-base \
    packagegroup-seco-gstreamer1.0-audio \
    packagegroup-seco-gstreamer1.0-video \
    packagegroup-seco-gstreamer1.0-network-base \
    packagegroup-seco-gstreamer1.0-debug \
"

IMAGE_INSTALL_NETWORKING += " \
    packagegroup-seco-networking \
"

IMAGE_INSTALL_QT5 += " \
    packagegroup-seco-qt5 \
    packagegroup-seco-qt5-tests \
    packagegroup-seco-qt5-tools \
    packagegroup-seco-qt5-translations \
    packagegroup-seco-qt5-examples \
"

IMAGE_INSTALL_QT6 += " \
    packagegroup-seco-qt6 \
    packagegroup-seco-qt6-tools \
    packagegroup-seco-qt6-translations \
"

IMAGE_INSTALL_FONTS += " \
    ttf-dejavu-common \
    ttf-dejavu-sans \
    ttf-dejavu-sans-mono \
    ttf-dejavu-sans-condensed \
    ttf-dejavu-serif \
    ttf-dejavu-serif-condensed \
    ttf-dejavu-mathtexgyre \
"


CORE_IMAGE_EXTRA_INSTALL += " \
    packagegroup-seco-debug \
    ${IMAGE_INSTALL_FONTS} \
    ${IMAGE_INSTALL_GRAPHICS} \
    ${IMAGE_INSTALL_TESTING} \
    ${IMAGE_INSTALL_MULTIMEDIA} \
    ${IMAGE_INSTALL_NETWORKING} \
    ${@bb.utils.contains('CLEA_OS_QT_VERSION', '5.x', '${IMAGE_INSTALL_QT5}', \
        bb.utils.contains('CLEA_OS_QT_VERSION', '6.x', '${IMAGE_INSTALL_QT6}', '', d), d)} \
"