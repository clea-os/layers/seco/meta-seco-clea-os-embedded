SUMMARY = "SECO Clea OS Image Base for Embedded"

require seco-image.inc

SPLASH_IMAGE = ""
SPLASH_IMAGE_FILE = ""

SPLASH_IMAGE:seco-arm = "u-boot-splash-image"
SPLASH_IMAGE_FILE:seco-arm = "splash.bmp"

DEPENDS += "${SPLASH_IMAGE}"
IMAGE_BOOT_FILES += "${SPLASH_IMAGE_FILE}"
