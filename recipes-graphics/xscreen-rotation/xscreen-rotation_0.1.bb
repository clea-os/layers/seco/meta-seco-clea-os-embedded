SUMMARY = "Custom Initialization Script"
DESCRIPTION = "Recipe to launch a custom script at boot time"

LICENSE = "CLOSED"

FILESEXTRAPATHS:prepend := "${THISDIR}/files:" 

RDEPENDS:${PN} += "bash"

SRC_URI = "file://xrotate-screen.sh"

#S = "${WORKDIR}"

inherit update-rc.d

INITSCRIPT_NAME = "xrotate-screen.sh"
INITSCRIPT_PARAMS = "start 99 2 3 4 5 . stop 20 0 1 6 ."

do_install() {
    install -d ${D}${sysconfdir}/init.d
    install -m 0755 ${WORKDIR}/xrotate-screen.sh ${D}${sysconfdir}/init.d/
}

FILES_${PN} = "${sysconfdir}/init.d/"

