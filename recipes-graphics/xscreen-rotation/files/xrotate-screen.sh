#!/bin/bash

# Define the file path
file_path="/sys/class/graphics/fbcon/rotate"

# Check if the file exists
if [ -e "$file_path" ]; then
	# Read the file content and store it in a variable
	file_content=$(<"$file_path")
	
	if [ "$file_content" -gt 0 ] && [ "$file_content" -le 4 ]; then
    		
		export DISPLAY=:0
		sleep 1

		case "$file_content" in
			1)
				xrandr -o right
				;;
			2)
				xrandr -o inverse
				;;
			3)
				xrandr -o left
				;;
		esac
	fi

fi
