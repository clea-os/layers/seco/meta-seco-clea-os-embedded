SUMMARY = "Qt simple paint"
DESCRIPTION = "A simple paint program created with Qt Creator in C++"
SECTION = "base"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${WORKDIR}/git/LICENSE.md;md5=841d5243c380d230e3e3d5d8522b161f"

SRC_URI = "git://github.com/vpapadopou/qt-simple-paint.git;protocol=https;branch=master"

PV = "1.0+git${SRCPV}"
SRCREV = "459796aa9cbf1ddaefa22cf8ba6b9fb5651e517d"

S = "${WORKDIR}/git"

DEPENDS += "qtbase"
RDEPENDS:${PN} += "bash"

inherit qt6-qmake

do_install() {
    install -d ${D}${bindir}
    install -m 0755 ${B}/simplePaint ${D}${bindir}/simplePaint-bin

    echo "#!/bin/bash"                       > ${D}${bindir}/simplePaint
    echo ": \${WAYLAND_DISPLAY:=wayland-0}" >> ${D}${bindir}/simplePaint
    echo ": \${DISPLAY:=:0.0}"              >> ${D}${bindir}/simplePaint
    echo ": \${QT_QPA_PLATFORM:=wayland}"   >> ${D}${bindir}/simplePaint
    echo "export WAYLAND_DISPLAY"           >> ${D}${bindir}/simplePaint
    echo "export DISPLAY"                   >> ${D}${bindir}/simplePaint
    echo "export QT_QPA_PLATFORM"           >> ${D}${bindir}/simplePaint
    echo "${bindir}/simplePaint-bin"        >> ${D}${bindir}/simplePaint
    chmod 0755 ${D}${bindir}/simplePaint
}

FILES:${PN} = "${bindir}"
