DESCRIPTION = "Frame Buffer Viewer"
LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=130f9d9dddfebd2c6ff59165f066e41c"

DEPENDS = "libpng jpeg"

# REFERENCE:
# https://git.openembedded.org/openembedded/tree/recipes/fbv
SRC_URI = " \
	git://repo.or.cz/fbv.git;protocol=https;branch=master \
	file://0001-CONFIGURE-support-yocto-cross-compilation.patch \
	file://0002-MAKE-fix-LIBS-CFLAGS.patch \
	file://0003-FBV-add-24bpp-BGR666-support.patch \
	file://0004-FBV-add-framebuffer-device-parameter-fix-bugs.patch \
"

PV = "1.0+git${SRCPV}"
SRCREV = "127dd84cede022cd5173ff2d7450677a14486784"

S = "${WORKDIR}/git"

CFLAGS += "-D_GNU_SOURCE -D__KERNEL_STRICT_NAMES"

CLEANBROKEN = "1"

do_configure() {
	cd ${S}
	rm -f Make.conf config.h config.log fbv *.o
	CC="${CC}" ./configure --without-libungif
}

do_compile() {
	oe_runmake CC="${CC}"
}

do_install() {
	install -d ${D}${bindir}
	install -m 0755 fbv ${D}${bindir}
}
