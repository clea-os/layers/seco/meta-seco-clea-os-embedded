require conf/distro/poky.conf

DISTRO = "seco-clea-os-embedded"
DISTRO_NAME = "SECO Clea OS Embedded Distro"

MAINTAINER = "SECO <seco.com>"

# Use systemd as default init manager
VIRTUAL-RUNTIME_init_manager = "systemd"
PREFERRED_PROVIDER_udev = "systemd"
PREFERRED_PROVIDER_udev-utils = "systemd"

# Add needed features
DISTRO_FEATURES:append = " systemd polkit modem ppp "

RUST_VERSION ?= "1.59.0"
PREFERRED_VERSION_cargo ?= "${RUST_VERSION}"
PREFERRED_VERSION_cargo-native ?= "${RUST_VERSION}"
PREFERRED_VERSION_libstd-rs ?= "${RUST_VERSION}"
PREFERRED_VERSION_rust ?= "${RUST_VERSION}"
PREFERRED_VERSION_rust-cross-${TARGET_ARCH} ?= "${RUST_VERSION}"
PREFERRED_VERSION_rust-llvm ?= "${RUST_VERSION}"
PREFERRED_VERSION_rust-llvm-native ?= "${RUST_VERSION}"
PREFERRED_VERSION_rust-native ?= "${RUST_VERSION}"


CLEA_OS_QT_VERSION ?= "5.x"

PREFERRED_VERSION_qt  ?= "${CLEA_OS_QT_VERSION}"

INHERIT += " clea-os-embedded "
